Categories:Science & Education
License:WTFPL
Web Site:
Source Code:https://github.com/c-base/c-beam-droid
Issue Tracker:https://github.com/c-base/c-beam-droid/issues

Auto Name:c-beam
Summary:Android interface to the c-base onboard computer c-beam
Description:
c-base on-board computer interface - control the c-base from your pocket!

NOTE: This app is only usable from within the c-base-crew wifi. If you don't
know what this means, don't download c-beam.

If you're still with us, here's a feature set for your informational pleasure:

* login / logout
* c-mission (task assignment)
* mind control
* world domination
* information retrieval
* current events
* c-portal
* artefacts
* local maps
* control
* c-leuse
* c_out
* and the rest of the space station
.

Repo Type:git
Repo:https://github.com/c-base/c-beam-droid

Build:1.5.2,25
    commit=1.5.2
    gradle=noGCM
    prebuild=rm libs/gcm.jar && \
        sed -i -e '/google-services/d'  -e '/withFirebaseCompile/d' build.gradle

Build:1.5.3,26
    commit=1.5.3
    gradle=noGCM
    prebuild=rm libs/gcm.jar && \
        sed -i -e '/google-services/d'  -e '/withFirebaseCompile/d' build.gradle

Build:1.6.1,28
    commit=1.6.1
    subdir=app
    gradle=noGCM
    prebuild=sed -i -e '/com.google.gms/d' ../build.gradle build.gradle && \
        sed -i -e '/withGCMImplementation/d' build.gradle

Build:1.6.2,29
    commit=1.6.2
    subdir=app
    gradle=noGCM
    prebuild=sed -i -e '/com.google.gms/d' ../build.gradle build.gradle && \
        sed -i -e '/withGCMImplementation/d' build.gradle

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.6.2
Current Version Code:29
